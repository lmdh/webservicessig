package fr.orleans.sig.WebServiceSIG.rest.controllers;

import fr.orleans.sig.WebServiceSIG.dto.*;
import fr.orleans.sig.WebServiceSIG.exceptions.ExpiredTokenException;
import fr.orleans.sig.WebServiceSIG.exceptions.InvalidTokenException;
import fr.orleans.sig.WebServiceSIG.services.SignalementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@RestController
@RequestMapping(path = "/signalement")
public class SignalementController {

    @Autowired
    SignalementService signalementService;

    @CrossOrigin
    @GetMapping(path ="/retard/{routeId}/{arretId}")
    public Collection<RetardDto> getRetard(@PathVariable String routeId, @PathVariable long arretId){
        return signalementService.getRetard(routeId,arretId);
    }

    @CrossOrigin
    @PostMapping(path = "/retard/{routeId}")
    public void postRetard (@PathVariable String  routeId, @RequestBody RetardDto retardDto,@RequestHeader(AUTHORIZATION) String accessToken) throws ExpiredTokenException, InvalidTokenException {
        signalementService.postRetard(routeId,retardDto,accessToken);
    }

    @CrossOrigin
    @GetMapping(path ="/coupure/{routeId}")
    public Collection<CoupureLigneDto> getCoupureLigne(@PathVariable String  routeId){
        return signalementService.getCoupureLigne(routeId);
    }

    @CrossOrigin
    @PostMapping(path = "/coupure/{routeId}")
    public void postCoupureLigne(@PathVariable String  routeId, @RequestBody CoupureLigneDto coupureLigneDto, @RequestHeader(AUTHORIZATION) String accessToken) throws ExpiredTokenException, InvalidTokenException {
        signalementService.postCoupureLigne(routeId,coupureLigneDto,accessToken);
    }

    @CrossOrigin
    @GetMapping(path = "/{routeId}/arrets")
    public Collection<ArretTaoDto> getArret(@PathVariable String  routeId){
        return signalementService.getArret(routeId);
    }

    @CrossOrigin
    @GetMapping(path = "/degradation/stationVelo/{id}")
    public Collection<DegradationStationVeloDto> getDegradationStationVelo(@PathVariable long id){
        return signalementService.getDegradationStationVelo(id);
    }

    @CrossOrigin
    @PostMapping(path = "/degradation/stationVelo/{id}")
    public void postDegradationStationVelo(@PathVariable long id, @RequestBody DegradationStationVeloDto degradationStationVeloDto, @RequestHeader(AUTHORIZATION) String accessToken) throws ExpiredTokenException, InvalidTokenException {
        signalementService.postDegradationStationVelo(id,degradationStationVeloDto,accessToken);
    }

    @CrossOrigin
    @GetMapping(path = "/degradation/stationnementVelo/{numero}")
    public Collection<DegradationStationnementVeloDto> getDegradationStationnementVelo(@PathVariable String numero){
        return signalementService.getDegradationStationnementVelo(numero);
    }

    @CrossOrigin
    @PostMapping(path = "/degradation/stationnementVelo/{numero}")
    public void postDegradationStationnementVelo(@PathVariable String numero, @RequestBody DegradationStationnementVeloDto degradationStationnementVeloDto, @RequestHeader(AUTHORIZATION) String accessToken) throws ExpiredTokenException, InvalidTokenException {
        signalementService.postDegradationStationnementVelo(numero,degradationStationnementVeloDto,accessToken);
    }

    @CrossOrigin
    @GetMapping(path = "/degradation/arretTao/{gid}")
    public Collection<DegradationArretTaoDto> getDegradationArretTao(@PathVariable long gid){
        return signalementService.getDegradationArretTao(gid);
    }

    @CrossOrigin
    @PostMapping(path = "/degradation/arretTao/{gid}")
    public void postDegradationArretTao(@PathVariable long gid, @RequestBody DegradationArretTaoDto arretTaoDto, @RequestHeader(AUTHORIZATION) String accessToken) throws ExpiredTokenException, InvalidTokenException {
        signalementService.postDegradationArretTao(gid,arretTaoDto,accessToken);
    }

    @CrossOrigin
    @GetMapping(path = "/degradation/pisteCyclable/{numero}/{insee}")
    public ResponseDto<Collection<DegradationPisteCyclableDto>> getDegradationPisteCyclable(@PathVariable String numero,@PathVariable String insee){
        Collection<DegradationPisteCyclableDto> degradationPisteCyclableCollection = signalementService.getDegradationPisteCyclable(numero,insee);
        return new ResponseDto<>(degradationPisteCyclableCollection,true);
    }

    @CrossOrigin
    @PostMapping(path = "/degradation/pisteCyclable/{numero}/{insee}")
    public void postDegradationPisteCyclable(@PathVariable String  numero, @PathVariable String insee, @RequestBody DegradationPisteCyclableDto degradationPisteCyclableDto,@RequestHeader(AUTHORIZATION) String accessToken) throws ExpiredTokenException, InvalidTokenException {
        signalementService.postDegradationPisteCyclable(numero,insee,degradationPisteCyclableDto,accessToken);
    }

    @CrossOrigin
    @GetMapping(path = "/penurie/{id}")
    public Collection<PenurieDto> getPenurie(@PathVariable long id){
        return signalementService.getPenurie(id);
    }

    @CrossOrigin
    @PostMapping(path = "/penurie/{id}")
    public void postPenurie(@PathVariable long id, @RequestBody PenurieDto penurieDto,@RequestHeader(AUTHORIZATION) String accessToken) throws ExpiredTokenException, InvalidTokenException {
        signalementService.postPenurie(id,penurieDto,accessToken);
    }
}

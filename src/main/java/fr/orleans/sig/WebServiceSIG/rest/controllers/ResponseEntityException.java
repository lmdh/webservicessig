package fr.orleans.sig.WebServiceSIG.rest.controllers;

import fr.orleans.sig.WebServiceSIG.dto.ResponseDto;
import fr.orleans.sig.WebServiceSIG.exceptions.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@ControllerAdvice
@RestController
public class ResponseEntityException extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ExpiredTokenException.class)
    public final ResponseEntity<String> handleExpiredTokenException(IncorrectLoginOrPasswordException e, WebRequest request) {
        String message = "Le token est expiré !";
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(message);
    }

    @ExceptionHandler(IncorrectLoginOrPasswordException.class)
    public final ResponseEntity<String> handleIncorrectLoginOrPasswordException(IncorrectLoginOrPasswordException e, WebRequest request) {
        String message = "Login ou mot de passe incorrect !";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message);
    }

    @ExceptionHandler(InvalidTokenException.class)
    public final ResponseEntity<String> handleInvalidTokenException(InvalidTokenException e, WebRequest request) {
        String message = "Le token n'est pas valide !";
        //return new ResponseDto<>(false, HttpStatus.UNAUTHORIZED.toString(), message);
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(message);
    }

    @ExceptionHandler(LoginAlreadyInUseException.class)
    public final ResponseEntity<String> handleLoginAlreadyInUseException(LoginAlreadyInUseException e, WebRequest request) {
        String message = "Le login spécifié est déjà pris !";
        return ResponseEntity.status(HttpStatus.CONFLICT).body(message);
    }

    @ExceptionHandler(MissingTokenException.class)
    public final ResponseEntity<String> handleMissingTokenException(MissingTokenException e, WebRequest request) {
        String message = "Token manquant !";
        //return new ResponseDto<>(false, HttpStatus.BAD_REQUEST.toString(), message);
        return ResponseEntity.status(HttpStatus.CONFLICT).body(message);
    }

    @ExceptionHandler(MissingRefreshTokenException.class)
    public final ResponseEntity<String> handleMissingRefreshTokenException(MissingRefreshTokenException e, WebRequest request) {
        String message = "Refresh token manquant !";
        //return new ResponseDto<>(false, HttpStatus.BAD_REQUEST.toString(), message);
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(message);
    }

    @ExceptionHandler(NullBodyException.class)
    public final ResponseEntity<String> handleNullBodyException(NullBodyException e, WebRequest request) {
        String message = "Un ou plusieurs champs vides !";
        //return new ResponseDto<>(false, HttpStatus.BAD_REQUEST.toString(), message);
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(message);
    }

    @ExceptionHandler(UserNotFoundException.class)
    public final ResponseEntity<String> handleUserNotFoundException(UserNotFoundException e, WebRequest request) {
        String message = "Nom d'utilisateur non trouvé";
        //return new ResponseDto<>(false, HttpStatus.BAD_REQUEST.toString(), message);
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(message);
    }
}

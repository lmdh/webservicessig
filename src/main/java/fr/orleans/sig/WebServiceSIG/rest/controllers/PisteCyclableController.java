package fr.orleans.sig.WebServiceSIG.rest.controllers;


import fr.orleans.sig.WebServiceSIG.dto.PisteCyclableDto;
import fr.orleans.sig.WebServiceSIG.dto.ResponseDto;
import fr.orleans.sig.WebServiceSIG.modele.PisteCyclable;
import fr.orleans.sig.WebServiceSIG.services.PisteCyclableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@RestController
@RequestMapping("/piste")
public class PisteCyclableController {
    @Autowired
    PisteCyclableService pisteCyclableService;

    @CrossOrigin
    @GetMapping(path = "/{gid}")
    public PisteCyclableDto getPiste(@PathVariable Integer gid) {
        System.out.println("In controller");
        PisteCyclableDto pisteCyclableDto = pisteCyclableService.getPiste(gid);
        return pisteCyclableDto;
    }

    @CrossOrigin
    @GetMapping(path = "/{gid}/geom")
    public List<Double[]> getPisteGeom(@PathVariable Integer gid) {
        List<Double[]> geom = pisteCyclableService.getPisteGeom(gid);
        return geom;
    }

    @CrossOrigin
    @GetMapping(path = "/all")
    public Collection<PisteCyclableDto> getAllPistes() {
        Collection<PisteCyclable> list = pisteCyclableService.getAllPistes();
        Collection<PisteCyclableDto> dtolist = new ArrayList<PisteCyclableDto>();
        for (PisteCyclable piste : list) {
            PisteCyclableDto temp = new PisteCyclableDto(piste.getGid(), piste.getNumero(), piste.getNumero(), piste.getQuartier(), piste.getRue(), piste.getLongueur(), piste.getSensCycl(), piste.getLongCycl(), piste.getTypeIti(), piste.getPosition(), piste.getLargeur(), piste.getRevetement(), piste.getCodcommune(), pisteCyclableService.getPisteGeom(piste.getGid()), pisteCyclableService.getPisteNote(piste.getGid()));
            dtolist.add(temp);
        }
        return dtolist;
    }
}

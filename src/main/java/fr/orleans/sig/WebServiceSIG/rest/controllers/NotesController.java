package fr.orleans.sig.WebServiceSIG.rest.controllers;

import fr.orleans.sig.WebServiceSIG.dto.NoteDto;
import fr.orleans.sig.WebServiceSIG.dto.ResponseDto;
import fr.orleans.sig.WebServiceSIG.exceptions.ExpiredTokenException;
import fr.orleans.sig.WebServiceSIG.exceptions.InvalidTokenException;
import fr.orleans.sig.WebServiceSIG.services.NotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.Collection;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@RestController
@RequestMapping("/notes")
public class NotesController {

    @Autowired
    NotesService notesService;

    @CrossOrigin
    @GetMapping(path ="/{idNote}")
    public NoteDto getNote(@PathVariable Integer idNote){
        NoteDto noteDto = new NoteDto();
        noteDto.setNote(notesService.getPisteNote(idNote));
        System.out.println("Reterning note  to client "+noteDto);
        return noteDto;
    }

    @CrossOrigin
    @PostMapping(path = "/{idNote}")
    public void postNote (@PathVariable Integer idNote, @RequestBody NoteDto noteDto,@RequestHeader(AUTHORIZATION) String accessToken) throws ExpiredTokenException, InvalidTokenException {
        notesService.postNote(idNote,noteDto,accessToken);
    }
}

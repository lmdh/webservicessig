package fr.orleans.sig.WebServiceSIG.rest.controllers;

import fr.orleans.sig.WebServiceSIG.dto.auth.AuthResultDto;
import fr.orleans.sig.WebServiceSIG.dto.auth.AuthRequestDto;
import fr.orleans.sig.WebServiceSIG.dto.ResponseDto;
import fr.orleans.sig.WebServiceSIG.dto.UtilisateurDto;
import fr.orleans.sig.WebServiceSIG.dto.auth.RefreshRequestDto;
import fr.orleans.sig.WebServiceSIG.exceptions.*;
import fr.orleans.sig.WebServiceSIG.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    @CrossOrigin
    @PostMapping("/register")
    public ResponseEntity<AuthResultDto> register(@RequestBody AuthRequestDto request) throws LoginAlreadyInUseException, NullBodyException {
        if (request != null) {
            AuthResultDto authResultDto = userService.register(request);
            return ResponseEntity.ok(authResultDto);
        } else {
            throw new NullBodyException("Champs vides");
        }
    }

    @CrossOrigin
    @PostMapping("/login")
    public ResponseEntity<AuthResultDto> login(@RequestBody AuthRequestDto request) throws IncorrectLoginOrPasswordException, NullBodyException {
        if (request != null) {
            AuthResultDto resultDto = userService.login(request);
            return ResponseEntity.ok(resultDto);
        } else {
            throw new NullBodyException("Champs vides");
        }
    }

    @CrossOrigin
    @PostMapping("/refresh")
    public ResponseEntity<AuthResultDto> refresh(@RequestBody RefreshRequestDto refreshRequest) throws ExpiredTokenException, InvalidTokenException, MissingRefreshTokenException {
        if (refreshRequest != null) {
            AuthResultDto authResultDto = userService.refresh(refreshRequest);
            return ResponseEntity.ok(authResultDto);
        } else {
            throw new MissingRefreshTokenException();
        }
    }

    @CrossOrigin
    @GetMapping("/me")
    public ResponseEntity<UtilisateurDto> me(@RequestHeader(AUTHORIZATION) String accessToken) throws ExpiredTokenException, InvalidTokenException, MissingTokenException {
        if (accessToken != null) {
            UtilisateurDto utilisateurDto = userService.getUserInfo(accessToken);
            return ResponseEntity.ok(utilisateurDto);
        } else {
            throw new MissingTokenException();
        }
    }

}

package fr.orleans.sig.WebServiceSIG.dao;

import fr.orleans.sig.WebServiceSIG.modele.DegradationStationVelo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DegradationStationVeloRepository extends JpaRepository<DegradationStationVelo, Long> {

    List<DegradationStationVelo> findByStationVeloDegrade_IdOrderByIdDegradationStationVeloDesc(long gid);
}

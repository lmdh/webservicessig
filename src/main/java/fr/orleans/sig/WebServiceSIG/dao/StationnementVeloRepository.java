package fr.orleans.sig.WebServiceSIG.dao;

import fr.orleans.sig.WebServiceSIG.modele.StationnementVelo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StationnementVeloRepository extends JpaRepository<StationnementVelo, Long> {

    StationnementVelo findByNumero(String numero);
}

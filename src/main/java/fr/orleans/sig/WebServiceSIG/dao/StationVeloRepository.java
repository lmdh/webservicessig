package fr.orleans.sig.WebServiceSIG.dao;

import fr.orleans.sig.WebServiceSIG.modele.StationVelo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StationVeloRepository extends JpaRepository<StationVelo, Long> {
}

package fr.orleans.sig.WebServiceSIG.dao;

import fr.orleans.sig.WebServiceSIG.modele.CoupureLigne;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CoupureLigneRepository extends JpaRepository<CoupureLigne, Long> {
}

package fr.orleans.sig.WebServiceSIG.dao;

import fr.orleans.sig.WebServiceSIG.modele.DegradationArretTao;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DegradationArretTaoRepository extends JpaRepository<DegradationArretTao, Long> {
    List<DegradationArretTao> findByArretTaoDegrade_Gid(long gid);
}

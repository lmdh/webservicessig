package fr.orleans.sig.WebServiceSIG.dao;

import fr.orleans.sig.WebServiceSIG.modele.Retard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Timestamp;
import java.util.List;

public interface RetardRepository extends JpaRepository<Retard, Long> {

    @Query(value = "from Retard rd where rd.ligneTaoConcerneRetard.routeId=:routeId and rd.idArret=:arretId and rd.date BETWEEN :startDate AND :endDate")
    public List<Object> getAllBetweenDates(@Param("startDate") Timestamp startDate, @Param("endDate")Timestamp endDate, @Param("routeId") String routeId, @Param("arretId") long arretId);
}

package fr.orleans.sig.WebServiceSIG.dao;

import fr.orleans.sig.WebServiceSIG.modele.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UtilisateurRepository extends JpaRepository<Utilisateur, String> {

}

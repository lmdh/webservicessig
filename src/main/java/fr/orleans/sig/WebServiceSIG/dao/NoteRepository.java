package fr.orleans.sig.WebServiceSIG.dao;

import fr.orleans.sig.WebServiceSIG.modele.Note;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NoteRepository extends JpaRepository<Note, Long> {
}

package fr.orleans.sig.WebServiceSIG.dao;

import fr.orleans.sig.WebServiceSIG.modele.Penurie;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PenurieRepository extends JpaRepository<Penurie, Long> {
    List<Penurie> findAllByStationVeloEnPenurie_IdOrderByDateDesc(Long id);
}

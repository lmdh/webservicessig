package fr.orleans.sig.WebServiceSIG.dao;

import fr.orleans.sig.WebServiceSIG.modele.DegradationStationVelo;
import fr.orleans.sig.WebServiceSIG.modele.DegradationStationnementVelo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DegradationStationnementVeloRepository extends JpaRepository<DegradationStationnementVelo, Long> {
    List<DegradationStationnementVelo> findByStationnementVeloDegrade_NumeroOrderByIdDegradationStationnementVeloDesc(String numero);
}

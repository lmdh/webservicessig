package fr.orleans.sig.WebServiceSIG.dao;

import fr.orleans.sig.WebServiceSIG.modele.DegradationPisteCyclable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DegradationPisteCyclableRepository extends JpaRepository<DegradationPisteCyclable, Long> {
}

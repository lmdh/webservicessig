package fr.orleans.sig.WebServiceSIG.dao;

import fr.orleans.sig.WebServiceSIG.modele.PisteCyclable;
import fr.orleans.sig.WebServiceSIG.modele.StationVelo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PisteCyclableRepository extends JpaRepository<PisteCyclable, Integer> {
    public PisteCyclable getPisteCyclableByGid(Integer gid);

}

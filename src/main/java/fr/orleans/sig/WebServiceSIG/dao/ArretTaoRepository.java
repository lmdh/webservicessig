package fr.orleans.sig.WebServiceSIG.dao;
import fr.orleans.sig.WebServiceSIG.modele.ArretTao;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArretTaoRepository extends JpaRepository<ArretTao, Long> {

}

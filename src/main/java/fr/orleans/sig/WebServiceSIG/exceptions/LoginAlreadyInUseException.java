package fr.orleans.sig.WebServiceSIG.exceptions;

public class LoginAlreadyInUseException extends Exception {

    public LoginAlreadyInUseException() {
    }

    public LoginAlreadyInUseException(String message) {
        super(message);
    }
}

package fr.orleans.sig.WebServiceSIG.exceptions;

public class ExpiredTokenException extends Exception {

    public ExpiredTokenException() {
    }

    public ExpiredTokenException(String message) {
        super(message);
    }
}

package fr.orleans.sig.WebServiceSIG.exceptions;

public class NullBodyException extends Exception {

    public NullBodyException() {
    }

    public NullBodyException(String message) {
        super(message);
    }
}

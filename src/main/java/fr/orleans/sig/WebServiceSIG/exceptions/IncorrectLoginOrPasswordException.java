package fr.orleans.sig.WebServiceSIG.exceptions;

public class IncorrectLoginOrPasswordException extends Exception {

    public IncorrectLoginOrPasswordException() {
    }

    public IncorrectLoginOrPasswordException(String message) {
        super(message);
    }
}

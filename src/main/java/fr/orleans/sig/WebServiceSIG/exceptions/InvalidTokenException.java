package fr.orleans.sig.WebServiceSIG.exceptions;

public class InvalidTokenException extends Exception {

    public InvalidTokenException() {};

    public InvalidTokenException(String message) {
        super(message);
    }

}

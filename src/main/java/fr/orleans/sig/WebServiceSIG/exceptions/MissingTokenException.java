package fr.orleans.sig.WebServiceSIG.exceptions;

public class MissingTokenException extends Exception {

    public MissingTokenException() {
    }

    public MissingTokenException(String message) {
        super(message);
    }
}

package fr.orleans.sig.WebServiceSIG.exceptions;

public class MissingRefreshTokenException extends Exception {

    public MissingRefreshTokenException() {
    }

    public MissingRefreshTokenException(String message) {
        super(message);
    }
}

package fr.orleans.sig.WebServiceSIG.dto;

import fr.orleans.sig.WebServiceSIG.modele.Penurie;
import fr.orleans.sig.WebServiceSIG.modele.StationVelo;
import fr.orleans.sig.WebServiceSIG.modele.Utilisateur;

import java.util.Date;

public class PenurieDto {

    private long idPenurie;

    private String message;

    private Date date;

    private UtilisateurDto declarateurPenurie;

    public PenurieDto(){}

    public PenurieDto(Penurie p){
        idPenurie = p.getIdPenurie();
        message = p.getMessage();
        date = p.getDate();
    }

    public PenurieDto(String message, Date date) {
        this.message = message;
        this.date = date;
    }

    public long getIdPenurie() {
        return idPenurie;
    }

    public void setIdPenurie(long idPenurie) {
        this.idPenurie = idPenurie;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    /*public Utilisateur getDeclarateurPenurie() {
        return declarateurPenurie;
    }

    public void setDeclarateurPenurie(Utilisateur declarateurPenurie) {
        this.declarateurPenurie = declarateurPenurie;
    }

    public StationVelo getStationVeloEnPenurie() {
        return stationVeloEnPenurie;
    }

    public void setStationVeloEnPenurie(StationVelo stationVeloEnPenurie) {
        this.stationVeloEnPenurie = stationVeloEnPenurie;
    }*/
}

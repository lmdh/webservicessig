package fr.orleans.sig.WebServiceSIG.dto;

import fr.orleans.sig.WebServiceSIG.modele.CoupureLigne;
import fr.orleans.sig.WebServiceSIG.modele.LigneTao;
import fr.orleans.sig.WebServiceSIG.modele.Utilisateur;

import java.util.Date;

public class CoupureLigneDto {

    private long idCoupureLigne;

    private String message;

    private long idArretDepart;

    private long idArretArrivee;

    private Date date;

    private Utilisateur declarateurCoupure;

    private LigneTao ligneTaoConcerne;

    public CoupureLigneDto(){}

    public CoupureLigneDto(String message, long idArretDepart, long idArretArrivee, Date date) {
        this.message = message;
        this.idArretDepart = idArretDepart;
        this.idArretArrivee = idArretArrivee;
        this.date = date;
    }

    public CoupureLigneDto(CoupureLigne c){
        idCoupureLigne = c.getIdCoupureLigne();
        message = c.getMessage();
        idArretDepart = c.getIdArretDepart();
        idArretArrivee = c.getIdArretArrivee();
        date = c.getDate();
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public long getIdCoupureLigne() {
        return idCoupureLigne;
    }

    public void setIdCoupureLigne(long idCoupureLigne) {
        this.idCoupureLigne = idCoupureLigne;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getIdArretDepart() {
        return idArretDepart;
    }

    public void setIdArretDepart(long idArretDepart) {
        this.idArretDepart = idArretDepart;
    }

    public long getIdArretArrivee() {
        return idArretArrivee;
    }

    public void setIdArretArrivee(long idArretArrivee) {
        this.idArretArrivee = idArretArrivee;
    }

    public Utilisateur getDeclarateurCoupure() {
        return declarateurCoupure;
    }

    public void setDeclarateurCoupure(Utilisateur declarateurCoupure) {
        this.declarateurCoupure = declarateurCoupure;
    }

    public LigneTao getLigneTaoConcerne() {
        return ligneTaoConcerne;
    }

    public void setLigneTaoConcerne(LigneTao ligneTaoConcerne) {
        this.ligneTaoConcerne = ligneTaoConcerne;
    }
}

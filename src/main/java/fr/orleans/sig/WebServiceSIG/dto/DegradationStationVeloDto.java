package fr.orleans.sig.WebServiceSIG.dto;

import fr.orleans.sig.WebServiceSIG.modele.DegradationStationVelo;
import fr.orleans.sig.WebServiceSIG.modele.StationVelo;
import fr.orleans.sig.WebServiceSIG.modele.Utilisateur;

import java.util.Date;

public class DegradationStationVeloDto {

    private long idDegradationStationVelo;

    private String message;

    private Date date;

    private byte[] photo;

    private UtilisateurDto declarateurDegradationStationVelo;

    private StationVelo stationVeloDegrade;

    public DegradationStationVeloDto(){}

    public DegradationStationVeloDto(String message, Date date, String photo, Utilisateur declarateurDegradationStationVelo, StationVelo stationVeloDegrade) {
        this.message = message;
        this.date = date;
        this.declarateurDegradationStationVelo = new UtilisateurDto(declarateurDegradationStationVelo);
        this.stationVeloDegrade = stationVeloDegrade;
        this.photo = photo.getBytes();
    }

    public DegradationStationVeloDto(DegradationStationVelo dsv){
        idDegradationStationVelo = dsv.getIdDegradationStationVelo();
        message = dsv.getMessage();
        date = dsv.getDate();
        photo = dsv.getPhoto();
    }

    public long getIdDegradationStationVelo() {
        return idDegradationStationVelo;
    }

    public void setIdDegradationStationVelo(long idDegradationStationVelo) {
        this.idDegradationStationVelo = idDegradationStationVelo;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public UtilisateurDto getDeclarateurDegradationStationVelo() {
        return declarateurDegradationStationVelo;
    }

    public void setDeclarateurDegradationStationVelo(UtilisateurDto declarateurDegradationStationVelo) {
        this.declarateurDegradationStationVelo = declarateurDegradationStationVelo;
    }

    public StationVelo getStationVeloDegrade() {
        return stationVeloDegrade;
    }

    public void setStationVeloDegrade(StationVelo stationVeloDegrade) {
        this.stationVeloDegrade = stationVeloDegrade;
    }
}

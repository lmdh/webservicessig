package fr.orleans.sig.WebServiceSIG.dto;

import fr.orleans.sig.WebServiceSIG.modele.DegradationStationnementVelo;
import fr.orleans.sig.WebServiceSIG.modele.StationnementVelo;
import fr.orleans.sig.WebServiceSIG.modele.Utilisateur;

import java.util.Date;

public class DegradationStationnementVeloDto {

    private long idDegradationStationnementVelo;

    private String message;

    private Date date;

    private byte[] photo;

    private Utilisateur declarateurDegradationStationnementVelo;

    private StationnementVelo stationnementVeloDegrade;

    public DegradationStationnementVeloDto(){}

    public DegradationStationnementVeloDto(DegradationStationnementVelo dsv){
        idDegradationStationnementVelo = dsv.getIdDegradationStationnementVelo();
        message = dsv.getMessage();
        date = dsv.getDate();
        photo = dsv.getPhoto();
    }

    public DegradationStationnementVeloDto(String message, Date date, String photo, Utilisateur declarateurDegradationStationnementVelo, StationnementVelo stationnementVeloDegrade) {
        this.message = message;
        this.date = date;
        this.photo = photo.getBytes();
        this.declarateurDegradationStationnementVelo = declarateurDegradationStationnementVelo;
        this.stationnementVeloDegrade = stationnementVeloDegrade;
    }

    public long getIdDegradationStationnementVelo() {
        return idDegradationStationnementVelo;
    }

    public void setIdDegradationStationnementVelo(long idDegradationStationnementVelo) {
        this.idDegradationStationnementVelo = idDegradationStationnementVelo;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public Utilisateur getDeclarateurDegradationStationnementVelo() {
        return declarateurDegradationStationnementVelo;
    }

    public void setDeclarateurDegradationStationnementVelo(Utilisateur declarateurDegradationStationnementVelo) {
        this.declarateurDegradationStationnementVelo = declarateurDegradationStationnementVelo;
    }

    public StationnementVelo getStationnementVeloDegrade() {
        return stationnementVeloDegrade;
    }

    public void setStationnementVeloDegrade(StationnementVelo stationnementVeloDegrade) {
        this.stationnementVeloDegrade = stationnementVeloDegrade;
    }
}

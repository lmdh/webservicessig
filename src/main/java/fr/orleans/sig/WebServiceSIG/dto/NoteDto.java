package fr.orleans.sig.WebServiceSIG.dto;

import fr.orleans.sig.WebServiceSIG.modele.Note;
import fr.orleans.sig.WebServiceSIG.modele.PisteCyclable;
import fr.orleans.sig.WebServiceSIG.modele.Utilisateur;

public class NoteDto {

    private long idNote;

    private float note;

    private PisteCyclable pisteCyclableConcerner;

    private Utilisateur proprietaire;

    public NoteDto(){}

    public NoteDto(Note n){
        idNote = n.getIdNote();
        note = n.getNote();
    }

    public NoteDto(int note) {
        this.note = note;
    }

    public long getIdNote() {
        return idNote;
    }

    public void setIdNote(long idNote) {
        this.idNote = idNote;
    }

    public float getNote() {
        return note;
    }

    public void setNote(float note) {
        this.note = note;
    }

    public PisteCyclable getPisteCyclableConcerner() {
        return pisteCyclableConcerner;
    }

    public void setPisteCyclableConcerner(PisteCyclable pisteCyclableConcerner) {
        this.pisteCyclableConcerner = pisteCyclableConcerner;
    }

    public Utilisateur getProprietaire() {
        return proprietaire;
    }

    public void setProprietaire(Utilisateur proprietaire) {
        this.proprietaire = proprietaire;
    }
}

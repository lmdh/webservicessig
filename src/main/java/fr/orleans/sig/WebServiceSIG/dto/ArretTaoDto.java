package fr.orleans.sig.WebServiceSIG.dto;

import fr.orleans.sig.WebServiceSIG.modele.ArretTao;


public class ArretTaoDto {

    private long gid;
    private String stopId;
    private String stopname;
    private String locationT;
    private String parentSta;

    public ArretTaoDto(){}

    public ArretTaoDto(String stopId, String stopname, String locationT, String parentSta) {
        this.stopId = stopId;
        this.stopname = stopname;
        this.locationT = locationT;
        this.parentSta = parentSta;
    }

    public ArretTaoDto(ArretTao arretTao){
        gid = arretTao.getGid();
        stopId = arretTao.getStopId();
        stopname = arretTao.getStopname();
        locationT = arretTao.getLocation_t();
        parentSta = arretTao.getParentSta();
    }

    public long getGid() {
        return gid;
    }

    public void setGid(long gid) {
        this.gid = gid;
    }


    public String getStopId() {
        return stopId;
    }

    public void setStopId(String stopId) {
        this.stopId = stopId;
    }


    public String getStopname() {
        return stopname;
    }

    public void setStopname(String stopname) {
        this.stopname = stopname;
    }


    public String getLocationT() {
        return locationT;
    }

    public void setLocationT(String locationT) {
        this.locationT = locationT;
    }


    public String getParentSta() {
        return parentSta;
    }

    public void setParentSta(String parentSta) {
        this.parentSta = parentSta;
    }

}

package fr.orleans.sig.WebServiceSIG.dto;


public class ResponseDto<T> {
    private T data;
    private boolean isSuccess;
    private String errorCode;
    private String errorMessage;

    public ResponseDto() {}

    public ResponseDto(boolean isSuccess) {
        this.isSuccess = isSuccess;
    }

    public ResponseDto(T data, boolean isSuccess) {
        this.data = data;
        this.isSuccess = isSuccess;
    }

    public ResponseDto(boolean isSuccess, String errorCode, String errorMessage) {
        this.isSuccess = isSuccess;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean success) {
        isSuccess = success;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}



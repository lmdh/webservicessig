package fr.orleans.sig.WebServiceSIG.dto;

import fr.orleans.sig.WebServiceSIG.modele.*;

import java.util.Collection;

public class UtilisateurDto {

    private String login;

    private Byte[] imgProfil;

    private Collection<Note> notes;

    private Collection<Retard> retardDeclare;

    private Collection<CoupureLigne> coupureLignesDeclare;

    private Collection<DegradationPisteCyclable> degradationPisteCyclablesDeclare;

    private Collection<DegradationStationnementVelo> degradationStationnementVelosDeclare;

    private Collection<DegradationArretTao> degradationArretTaosDeclare;

    private Collection<DegradationStationVelo> degradationStationVelosDeclare;

    private Collection<Penurie> penuriesDeclare;

    public UtilisateurDto(String login, Byte[] imgProfil, Collection<Note> notes, Collection<Retard> retardDeclare, Collection<CoupureLigne> coupureLignesDeclare, Collection<DegradationPisteCyclable> degradationPisteCyclablesDeclare, Collection<DegradationStationnementVelo> degradationStationnementVelosDeclare, Collection<DegradationArretTao> degradationArretTaosDeclare, Collection<DegradationStationVelo> degradationStationVelosDeclare, Collection<Penurie> penuriesDeclare) {
        this.login = login;
        this.imgProfil = imgProfil;
        this.notes = notes;
        this.retardDeclare = retardDeclare;
        this.coupureLignesDeclare = coupureLignesDeclare;
        this.degradationPisteCyclablesDeclare = degradationPisteCyclablesDeclare;
        this.degradationStationnementVelosDeclare = degradationStationnementVelosDeclare;
        this.degradationArretTaosDeclare = degradationArretTaosDeclare;
        this.degradationStationVelosDeclare = degradationStationVelosDeclare;
        this.penuriesDeclare = penuriesDeclare;
    }

    public UtilisateurDto(Utilisateur u) {
        this.login = u.getLogin();
        this.imgProfil = u.getImgProfil();
        this.notes = u.getNotes();
        this.retardDeclare = u.getRetardDeclare();
        this.coupureLignesDeclare = u.getCoupureLignesDeclare();
        this.degradationPisteCyclablesDeclare = u.getDegradationPisteCyclablesDeclare();
        this.degradationStationnementVelosDeclare = u.getDegradationStationnementVelosDeclare();
        this.degradationArretTaosDeclare = u.getDegradationArretTaosDeclare();
        this.degradationStationVelosDeclare = u.getDegradationStationVelosDeclare();
        this.penuriesDeclare = u.getPenuriesDeclare();
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Byte[] getImgProfil() {
        return imgProfil;
    }

    public void setImgProfil(Byte[] imgProfil) {
        this.imgProfil = imgProfil;
    }

    public Collection<Note> getNotes() {
        return notes;
    }

    public void setNotes(Collection<Note> notes) {
        this.notes = notes;
    }

    public Collection<Retard> getRetardDeclare() {
        return retardDeclare;
    }

    public void setRetardDeclare(Collection<Retard> retardDeclare) {
        this.retardDeclare = retardDeclare;
    }

    public Collection<CoupureLigne> getCoupureLignesDeclare() {
        return coupureLignesDeclare;
    }

    public void setCoupureLignesDeclare(Collection<CoupureLigne> coupureLignesDeclare) {
        this.coupureLignesDeclare = coupureLignesDeclare;
    }

    public Collection<DegradationPisteCyclable> getDegradationPisteCyclablesDeclare() {
        return degradationPisteCyclablesDeclare;
    }

    public void setDegradationPisteCyclablesDeclare(Collection<DegradationPisteCyclable> degradationPisteCyclablesDeclare) {
        this.degradationPisteCyclablesDeclare = degradationPisteCyclablesDeclare;
    }

    public Collection<DegradationStationnementVelo> getDegradationStationnementVelosDeclare() {
        return degradationStationnementVelosDeclare;
    }

    public void setDegradationStationnementVelosDeclare(Collection<DegradationStationnementVelo> degradationStationnementVelosDeclare) {
        this.degradationStationnementVelosDeclare = degradationStationnementVelosDeclare;
    }

    public Collection<DegradationArretTao> getDegradationArretTaosDeclare() {
        return degradationArretTaosDeclare;
    }

    public void setDegradationArretTaosDeclare(Collection<DegradationArretTao> degradationArretTaosDeclare) {
        this.degradationArretTaosDeclare = degradationArretTaosDeclare;
    }

    public Collection<DegradationStationVelo> getDegradationStationVelosDeclare() {
        return degradationStationVelosDeclare;
    }

    public void setDegradationStationVelosDeclare(Collection<DegradationStationVelo> degradationStationVelosDeclare) {
        this.degradationStationVelosDeclare = degradationStationVelosDeclare;
    }

    public Collection<Penurie> getPenuriesDeclare() {
        return penuriesDeclare;
    }

    public void setPenuriesDeclare(Collection<Penurie> penuriesDeclare) {
        this.penuriesDeclare = penuriesDeclare;
    }
}

package fr.orleans.sig.WebServiceSIG.dto;

import fr.orleans.sig.WebServiceSIG.modele.DegradationPisteCyclable;
import fr.orleans.sig.WebServiceSIG.modele.PisteCyclable;
import fr.orleans.sig.WebServiceSIG.modele.Utilisateur;

import java.util.Date;

public class DegradationPisteCyclableDto {

    private long idDegradationPisteCyclable;

    private String message;

    private Date date;

    private byte[] photo;

    private Utilisateur declarateurDegradationPisteCyclable;

    private PisteCyclable pisteCyclableDegradeConcerner;

    public DegradationPisteCyclableDto(){}

    public DegradationPisteCyclableDto(DegradationPisteCyclable dpc){
        idDegradationPisteCyclable = dpc.getIdDegradationPisteCyclable();
        message = dpc.getMessage();
        date = dpc.getDate();
        photo = dpc.getPhoto();
    }

    public DegradationPisteCyclableDto(String message, Date date, String photo) {
        this.message = message;
        this.date = date;
        this.photo = photo.getBytes();

    }

    public long getIdDegradationPisteCyclable() {
        return idDegradationPisteCyclable;
    }

    public void setIdDegradationPisteCyclable(long idDegradationPisteCyclable) {
        this.idDegradationPisteCyclable = idDegradationPisteCyclable;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public Utilisateur getDeclarateurDegradationPisteCyclable() {
        return declarateurDegradationPisteCyclable;
    }

    public void setDeclarateurDegradationPisteCyclable(Utilisateur declarateurDegradationPisteCyclable) {
        this.declarateurDegradationPisteCyclable = declarateurDegradationPisteCyclable;
    }

    public PisteCyclable getPisteCyclableDegradeConcerner() {
        return pisteCyclableDegradeConcerner;
    }

    public void setPisteCyclableDegradeConcerner(PisteCyclable pisteCyclableDegradeConcerner) {
        this.pisteCyclableDegradeConcerner = pisteCyclableDegradeConcerner;
    }
}

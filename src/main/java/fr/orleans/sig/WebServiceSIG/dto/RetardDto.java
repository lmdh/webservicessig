package fr.orleans.sig.WebServiceSIG.dto;

import fr.orleans.sig.WebServiceSIG.modele.LigneTao;
import fr.orleans.sig.WebServiceSIG.modele.Retard;
import fr.orleans.sig.WebServiceSIG.modele.Utilisateur;

import java.util.Date;

public class RetardDto {

    private long idRetard;

    private String message;

    private long duree;

    private long idArret;

    private Date date;

    private Utilisateur declarateurRetard;

    private LigneTao ligneTaoConcerneRetard;

    public RetardDto(){}

    public RetardDto(String message, long duree, long idArret, Date date) {
        this.message = message;
        this.duree = duree;
        this.idArret = idArret;
        this.date = date;
    }

    public RetardDto(Retard r){
        idRetard = r.getIdRetard();
        message = r.getMessage();
        duree = r.getDuree();
        idArret = r.getIdArret();
        date = r.getDate();
    }

    public long getIdRetard() {
        return idRetard;
    }

    public void setIdRetard(long idRetard) {
        this.idRetard = idRetard;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getDuree() {
        return duree;
    }

    public void setDuree(long duree) {
        this.duree = duree;
    }

    public long getIdArret() {
        return idArret;
    }

    public void setIdArret(long idArret) {
        this.idArret = idArret;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Utilisateur getDeclarateurRetard() {
        return declarateurRetard;
    }

    public void setDeclarateurRetard(Utilisateur declarateurRetard) {
        this.declarateurRetard = declarateurRetard;
    }

    public LigneTao getLigneTaoConcerneRetard() {
        return ligneTaoConcerneRetard;
    }

    public void setLigneTaoConcerneRetard(LigneTao ligneTaoConcerneRetard) {
        this.ligneTaoConcerneRetard = ligneTaoConcerneRetard;
    }
}

package fr.orleans.sig.WebServiceSIG.dto;

import fr.orleans.sig.WebServiceSIG.modele.DegradationPisteCyclable;
import fr.orleans.sig.WebServiceSIG.modele.Note;
import org.postgis.MultiLineString;

import java.util.Collection;
import java.util.List;

public class PisteCyclableDto {

    private Integer gid;

    private String numero;

    private String commune;

    private String quartier;

    private String rue;

    private String longueur;

    private String sensCycl;

    private String longCycl;

    private String typeIti;

    private String position;

    private String largeur;

    private String revetement;

    private String codcommune;

    private List<Double[]> geom;

    private float note;

    public PisteCyclableDto() {
    }

    public PisteCyclableDto(Integer gid, String numero, String commune, String quartier, String rue, String longueur, String sensCycl, String longCycl, String typeIti, String position, String largeur, String revetement, String codcommune, List<Double[]> geom, float note) {
        this.gid = gid;
        this.numero = numero;
        this.commune = commune;
        this.quartier = quartier;
        this.rue = rue;
        this.longueur = longueur;
        this.sensCycl = sensCycl;
        this.longCycl = longCycl;
        this.typeIti = typeIti;
        this.position = position;
        this.largeur = largeur;
        this.revetement = revetement;
        this.codcommune = codcommune;
        this.geom = geom;
        this.note = note;
    }

    public Integer getGid() {
        return gid;
    }

    public void setGid(Integer gid) {
        this.gid = gid;
    }


    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }


    public String getCommune() {
        return commune;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }


    public String getQuartier() {
        return quartier;
    }

    public void setQuartier(String quartier) {
        this.quartier = quartier;
    }


    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }


    public String getLongueur() {
        return longueur;
    }

    public void setLongueur(String longueur) {
        this.longueur = longueur;
    }


    public String getSensCycl() {
        return sensCycl;
    }

    public void setSensCycl(String sensCycl) {
        this.sensCycl = sensCycl;
    }


    public String getLongCycl() {
        return longCycl;
    }

    public void setLongCycl(String longCycl) {
        this.longCycl = longCycl;
    }


    public String getTypeIti() {
        return typeIti;
    }

    public void setTypeIti(String typeIti) {
        this.typeIti = typeIti;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }


    public String getLargeur() {
        return largeur;
    }

    public void setLargeur(String largeur) {
        this.largeur = largeur;
    }


    public String getRevetement() {
        return revetement;
    }

    public void setRevetement(String revetement) {
        this.revetement = revetement;
    }

    public String getCodcommune() {
        return codcommune;
    }

    public void setCodcommune(String codcommune) {
        this.codcommune = codcommune;
    }

    public List<Double[]> getGeom() { return geom; }

    public void setGeom(List<Double[]> geom) { this.geom = geom; }

    public float getNote() { return note; }

    public void setNote(float note) { this.note = note; }
}

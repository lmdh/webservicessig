package fr.orleans.sig.WebServiceSIG.dto;

import fr.orleans.sig.WebServiceSIG.modele.ArretTao;
import fr.orleans.sig.WebServiceSIG.modele.DegradationArretTao;
import fr.orleans.sig.WebServiceSIG.modele.Utilisateur;

import java.util.Date;

public class DegradationArretTaoDto {

    private long idDegradationArretTao;

    private String message;

    private Date date;

    private byte[] photo;

    private Utilisateur declarateurDegradationArretTao;

    private ArretTao arretTaoDegrade;

    public DegradationArretTaoDto(){}

    public DegradationArretTaoDto(DegradationArretTao dat){
        idDegradationArretTao = dat.getIdDegradationArretTao();
        message = dat.getMessage();
        date = dat.getDate();
        photo = dat.getPhoto();
    }

    public DegradationArretTaoDto(String message, Date date, String photo, Utilisateur declarateurDegradationArretTao, ArretTao arretTaoDegrade) {
        this.message = message;
        this.date = date;
        this.photo = photo.getBytes();
        this.declarateurDegradationArretTao = declarateurDegradationArretTao;
        this.arretTaoDegrade = arretTaoDegrade;
    }

    public long getIdDegradationArretTao() {
        return idDegradationArretTao;
    }

    public void setIdDegradationArretTao(long idDegradationArretTao) {
        this.idDegradationArretTao = idDegradationArretTao;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public Utilisateur getDeclarateurDegradationArretTao() {
        return declarateurDegradationArretTao;
    }

    public void setDeclarateurDegradationArretTao(Utilisateur declarateurDegradationArretTao) {
        this.declarateurDegradationArretTao = declarateurDegradationArretTao;
    }

    public ArretTao getArretTaoDegrade() {
        return arretTaoDegrade;
    }

    public void setArretTaoDegrade(ArretTao arretTaoDegrade) {
        this.arretTaoDegrade = arretTaoDegrade;
    }
}

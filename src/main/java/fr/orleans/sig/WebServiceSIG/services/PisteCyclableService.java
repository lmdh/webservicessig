package fr.orleans.sig.WebServiceSIG.services;

import fr.orleans.sig.WebServiceSIG.dao.PisteCyclableRepository;
import fr.orleans.sig.WebServiceSIG.dto.PisteCyclableDto;
import fr.orleans.sig.WebServiceSIG.modele.PisteCyclable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;


@Service
public class PisteCyclableService {
    @PersistenceContext
    EntityManager em;

    @Autowired
    PisteCyclableRepository pisteCyclableRepository;
    NotesService notesService;

    /*
        Méthode permettant de récuperer une piste cyclable en fonction de son id
     */
    public PisteCyclableDto getPiste(Integer gid) {
        try {
            PisteCyclable pc = pisteCyclableRepository.getPisteCyclableByGid(gid);
            List<Double[]> geom = this.getPisteGeom(gid);
            float note = notesService.getPisteNote(gid);
            PisteCyclableDto pisteCyclableDto = new PisteCyclableDto(pc.getGid(), pc.getNumero(), pc.getCommune(), pc.getQuartier(), pc.getRue(), pc.getLongueur(), pc.getSensCycl(), pc.getLongCycl(), pc.getTypeIti(), pc.getPosition(), pc.getLargeur(), pc.getRevetement(), pc.getCodcommune(), geom, note);
            return pisteCyclableDto;
        } catch (Exception e) {
            e.printStackTrace();
            return new PisteCyclableDto();
        }
    }

    /*
        Méthode permettant de récupérer le geom d'une piste
     */
    public List<Double[]> getPisteGeom(Integer gid) {
        try {
            Query q = em.createNativeQuery("SELECT ST_X(st_astext(((ST_Dumppoints (geom)).geom))),ST_Y(st_astext(((ST_Dumppoints (geom)).geom))) from piste_cyclable pc where pc.gid=?");
            q.setParameter(1, gid);
            List<Double[]> resultList = (List<Double[]>) q.getResultList();
            return resultList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /*
        Métode permettant de récupérer toutes les pistes cyclable
     */
    public Collection<PisteCyclable> getAllPistes() {
        return pisteCyclableRepository.findAll();
    }

    /*
        Méthode permettant de récupérer la moyenne des notes d'une piste
     */
    public float getPisteNote(Integer gid) {
        Query q = em.createNativeQuery("SELECT AVG(note) from note nt where nt.piste_cyclable_concerner_gid=?");
        q.setParameter(1, gid);
        BigDecimal val = (BigDecimal) q.getSingleResult();
        if (val != null) return (float) Math.round(val.floatValue());
        return 0;
    }
}

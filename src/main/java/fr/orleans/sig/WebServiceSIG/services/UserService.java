package fr.orleans.sig.WebServiceSIG.services;

import fr.orleans.sig.WebServiceSIG.dao.UtilisateurRepository;
import fr.orleans.sig.WebServiceSIG.dto.auth.AuthRequestDto;
import fr.orleans.sig.WebServiceSIG.dto.auth.AuthResultDto;
import fr.orleans.sig.WebServiceSIG.dto.UtilisateurDto;
import fr.orleans.sig.WebServiceSIG.dto.auth.RefreshRequestDto;
import fr.orleans.sig.WebServiceSIG.exceptions.*;
import fr.orleans.sig.WebServiceSIG.modele.Utilisateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UtilisateurRepository utilisateurRepository;

    @Autowired
    TokenService tokenService;

    /*
        Méthode permettant de hasher le mot de passe entré par l'utilisateur afin de le comparé avec celui stocké dans la base de donnée
     */
    private String getSecurePassword(String password) {
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] bytes = md.digest(password.getBytes());
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return generatedPassword;
    }

    /*
        Méthode permettant de verifier si lors de la création d'un compte utilisateur, le login qu'il rentre n'existe pas déja dans la base
     */
    public boolean userAlreadyExists(String login) {
        return utilisateurRepository.findById(login).isPresent();
    }

    /*
        Méthode permettant d'enregistrer un utilisateur dans la base de donnée
     */
    public AuthResultDto register(AuthRequestDto request) throws LoginAlreadyInUseException {
        if (userAlreadyExists(request.getLogin())) {
            throw new LoginAlreadyInUseException();
        } else {
            Utilisateur u = new Utilisateur();
            u.setLogin(request.getLogin());
            u.setMdp(getSecurePassword(request.getPassword()));
            utilisateurRepository.save(u);
            return tokenService.generateTokens(request.getLogin());
        }
    }

    /*
        Méthode permettant de verifier si l'utilisateur existe dans la base de donnée
     */
    private boolean validPassword(Utilisateur u, String password) {
        String securedPassword = getSecurePassword(password);
        String bdPassword = u.getMdp();
        return securedPassword.equals(bdPassword);
    }

    /*
        Méthode permettant de connecter un utilisateur existant
     */
    public AuthResultDto login(AuthRequestDto request) throws IncorrectLoginOrPasswordException {
        Optional<Utilisateur> query = utilisateurRepository.findById(request.getLogin());
        if (query.isPresent()) {
            Utilisateur u = query.get();
            if (validPassword(u, request.getPassword())) {
                return tokenService.generateTokens(request.getLogin());
            } else {
                throw new IncorrectLoginOrPasswordException();
            }
        } else {
            throw new IncorrectLoginOrPasswordException();
        }
    }

    /*
        Méthode permettant de récupèrer les informations de l'utilisateur
     */
    public UtilisateurDto getUserInfo(String accessToken) throws ExpiredTokenException, InvalidTokenException {
        String login = tokenService.checkToken(accessToken);
        Optional<Utilisateur> query = utilisateurRepository.findById(login);
        if (query.isPresent()) {
            Utilisateur u = query.get();
            return new UtilisateurDto(u);
        } else {
            throw new InvalidTokenException();
        }
    }

    /*
        Méthode qui retourne le refresh token
     */
    public AuthResultDto refresh(RefreshRequestDto refreshRequest) throws InvalidTokenException, ExpiredTokenException {
        return tokenService.refreshToken(refreshRequest.getRefreshToken());
    }
}

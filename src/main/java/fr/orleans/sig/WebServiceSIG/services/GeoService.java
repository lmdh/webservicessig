package fr.orleans.sig.WebServiceSIG.services;

import fr.orleans.sig.WebServiceSIG.dto.ArretTaoDto;
import fr.orleans.sig.WebServiceSIG.modele.ArretTao;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class GeoService {

    @PersistenceContext
    EntityManager em;

    public Collection<ArretTaoDto> getAllArrets() {
        Query q = em.createQuery("from ArretTao a");
        List arrettaoCollection = q.getResultList();
        Collection<ArretTaoDto> arretTaoDtos = new ArrayList<>();
        for (Object o : arrettaoCollection) {
            ArretTao a = (ArretTao) o;
            arretTaoDtos.add(new ArretTaoDto(a));
        }
        return arretTaoDtos;
    }
}

package fr.orleans.sig.WebServiceSIG.services;

import fr.orleans.sig.WebServiceSIG.dao.UtilisateurRepository;
import fr.orleans.sig.WebServiceSIG.dto.auth.AuthResultDto;
import fr.orleans.sig.WebServiceSIG.exceptions.ExpiredTokenException;
import fr.orleans.sig.WebServiceSIG.exceptions.InvalidTokenException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class TokenService {

    private final String SECRET_KEY = "hjsqfsqsjq";
    private final String REFRESH_KEY = "skfjsdkjjsd";
    //Valide pendant 1h
    private final int EXPIRATION_TIME = 3600000;
    //Valide pendant 10 heures
    private final int REFRESH_TOKEN_EXPIRATION_TIME = 36000000;
    private static final String TOKEN_PREFIX = "Bearer";

    @Autowired
    UtilisateurRepository utilisateurRepository;

    /*
        Mèthode générant le token
     */
    AuthResultDto generateTokens(String login) {
        Claims claims = Jwts.claims().setSubject(login);
        String token = Jwts.builder()
                .setClaims(claims)
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, SECRET_KEY.getBytes())
                .compact();
        token = TOKEN_PREFIX + token;
        String refresh = Jwts.builder()
                .setClaims(claims)
                .setExpiration(new Date(System.currentTimeMillis() + REFRESH_TOKEN_EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, REFRESH_KEY.getBytes())
                .compact();
        return new AuthResultDto(token, refresh, EXPIRATION_TIME);
    }

    /*
        Mètode permettant de vérifier si le token est valide
     */
    String checkToken(String token) throws ExpiredTokenException, InvalidTokenException {
            try {
                Jws<Claims> jwsClaims = Jwts.parser()
                        .setSigningKey(SECRET_KEY.getBytes())
                        .parseClaimsJws(token.replaceFirst(TOKEN_PREFIX, ""));
                if (jwsClaims.getBody().getExpiration().getTime() < new Date(System.currentTimeMillis()).getTime()) {
                    throw new ExpiredTokenException();
                } else {
                    String login = jwsClaims.getBody().getSubject();
                    if (utilisateurRepository.findById(login).isPresent()) {
                        return login;
                    } else
                        throw new InvalidTokenException();
                }
            } catch (ExpiredTokenException e) {
                throw e;
            } catch (Exception f) {
                throw new InvalidTokenException();
            }
    }

    /*
        Méthode permettanr de génerer le refresh token
     */
    public AuthResultDto refreshToken(String refreshToken) throws ExpiredTokenException, InvalidTokenException {
        Jws<Claims> jwsClaims = Jwts.parser()
                .setSigningKey(REFRESH_KEY.getBytes())
                .parseClaimsJws(refreshToken);
        if (jwsClaims.getBody().getExpiration().getTime() < new Date(System.currentTimeMillis()).getTime()) {
            throw new ExpiredTokenException();
        } else {
            String login = jwsClaims.getBody().getSubject();
            if (utilisateurRepository.findById(login).isPresent()) {
                return generateTokens(login);
            } else {
                throw new InvalidTokenException();
            }
        }
    }
}

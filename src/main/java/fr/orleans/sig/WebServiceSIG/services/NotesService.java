package fr.orleans.sig.WebServiceSIG.services;

import fr.orleans.sig.WebServiceSIG.dto.DegradationPisteCyclableDto;
import fr.orleans.sig.WebServiceSIG.dto.NoteDto;
import fr.orleans.sig.WebServiceSIG.exceptions.ExpiredTokenException;
import fr.orleans.sig.WebServiceSIG.exceptions.InvalidTokenException;
import fr.orleans.sig.WebServiceSIG.modele.DegradationPisteCyclable;
import fr.orleans.sig.WebServiceSIG.modele.Note;
import fr.orleans.sig.WebServiceSIG.modele.PisteCyclable;
import fr.orleans.sig.WebServiceSIG.modele.Utilisateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class NotesService {

    @PersistenceContext
    EntityManager em;

    @Autowired
    TokenService tokenService;

    /*
        Méthode permettant de récupèrer la liste des notes sur une piste cyclable
     */
    public Collection<NoteDto> getNotes(Integer gid) {
        try {
            Query q2 = em.createQuery("from Note n where n.pisteCyclableConcerner.gid=:id");
            q2.setParameter("id", gid);
            List resultQ2 = q2.getResultList();
            ArrayList<NoteDto> notePisteCyclableDtos = new ArrayList<>();
            for (Object o : resultQ2) {
                Note n = (Note) o;
                notePisteCyclableDtos.add(new NoteDto(n));
            }
            return notePisteCyclableDtos;
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    /*
        Méthode permettant de poster une note par rapport à une piste cyclable
     */
    @Transactional
    public void postNote(Integer gid, NoteDto noteDto, String accessToken) throws ExpiredTokenException, InvalidTokenException {
        String token = tokenService.checkToken(accessToken);
        Utilisateur u = em.find(Utilisateur.class, token);
        Query q = em.createQuery("from PisteCyclable pc where pc.gid=:id");
        q.setParameter("id", gid);
        List resultQ = q.getResultList();
        PisteCyclable pc = (PisteCyclable) resultQ.get(0);
        noteDto.setProprietaire(u);
        noteDto.setPisteCyclableConcerner(pc);
        Note n = new Note(noteDto);
        em.persist(n);
    }

    /*
        Méthode permettant de récupérer la moyenne des notes sur une piste cyclable
     */
    public float getPisteNote(Integer gid) {
        Query q = em.createNativeQuery("SELECT AVG(note) from note nt where nt.piste_cyclable_concerner_gid=?");
        q.setParameter(1, gid);
        BigDecimal val = (BigDecimal) q.getSingleResult();
        if (val != null) return (float) Math.round(val.floatValue());
        return 0;
    }
}

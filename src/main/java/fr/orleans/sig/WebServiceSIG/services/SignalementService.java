package fr.orleans.sig.WebServiceSIG.services;

import fr.orleans.sig.WebServiceSIG.dao.*;
import fr.orleans.sig.WebServiceSIG.dto.*;
import fr.orleans.sig.WebServiceSIG.exceptions.ExpiredTokenException;
import fr.orleans.sig.WebServiceSIG.exceptions.InvalidTokenException;
import fr.orleans.sig.WebServiceSIG.modele.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Service
public class SignalementService {

    private EntityManager em;

    private TokenService tokenService;

    private ArretTaoRepository arretTaoRepository;

    private StationVeloRepository stationVeloRepository;

    private RetardRepository retardRepository;

    private DegradationStationVeloRepository degradationStationVeloRepository;

    private DegradationStationnementVeloRepository degradationStationnementVeloRepository;

    private PenurieRepository penurieRepository;

    @Autowired
    public SignalementService(EntityManager em, TokenService tokenService, ArretTaoRepository arretTaoRepository, StationVeloRepository stationVeloRepository,
                              DegradationStationVeloRepository degradationStationVeloRepository, DegradationArretTaoRepository degradationArretTaoRepository,
                              PenurieRepository penurieRepository, DegradationStationnementVeloRepository degradationStationnementVeloRepository,
                              RetardRepository retardRepository) {
        this.em = em;
        this.tokenService = tokenService;
        this.arretTaoRepository = arretTaoRepository;
        this.stationVeloRepository = stationVeloRepository;
        this.degradationStationVeloRepository = degradationStationVeloRepository;
        this.degradationStationnementVeloRepository = degradationStationnementVeloRepository;
        this.penurieRepository = penurieRepository;
        this.retardRepository = retardRepository;
    }

    /*
        Méthode permettant de récupérer les retard du jour pour un arret donné en paramètre
     */
    public Collection<RetardDto> getRetard(String routeId, long idArret) {
        try {
            Instant now = Instant.now();
            Instant yesterday = now.minus(1, ChronoUnit.DAYS);
            Instant tomorrow = now.plus(1, ChronoUnit.DAYS);
            List<Object> ret = retardRepository.getAllBetweenDates(Timestamp.from(yesterday), Timestamp.from(tomorrow), routeId, idArret);
            ArrayList<RetardDto> reponse = new ArrayList<>();
            if (!ret.isEmpty()) {
                for (Object o : ret) {
                    Retard r = (Retard) o;
                    if (r.getLigneTaoConcerneRetard().getRouteId().equals(routeId))
                        reponse.add(new RetardDto(r));
                }
            }
            return reponse;
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    /*
        Méthode permettant de push un retard dans la base de donnée. Elle prend en paramètre l'id de la route, le dto du retard à push
     */
    @Transactional
    public void postRetard(String routeId, RetardDto retardDto, String accesToken) throws ExpiredTokenException, InvalidTokenException {
        String token = tokenService.checkToken(accesToken);
        Utilisateur u = em.find(Utilisateur.class, token);
        Query q = em.createQuery("from LigneTao l where l.routeId=:r");
        q.setParameter("r", routeId);
        List resutlQ = q.getResultList();
        LigneTao ligneTao = (LigneTao) resutlQ.get(0);
        retardDto.setDeclarateurRetard(u);
        retardDto.setLigneTaoConcerneRetard(ligneTao);
        Retard retard = new Retard(retardDto);
        em.persist(retard);
    }

    /*
        Méthode permettant de récuper les coupures de lignes en utilisant l'id de la ligne en paramètre
     */
    public Collection<CoupureLigneDto> getCoupureLigne(String routeId) {
        try {
            Query q = em.createQuery("from LigneTao l where l.routeId=:r");
            q.setParameter("r", routeId);
            List resutlQ = q.getResultList();
            LigneTao ligneTao = (LigneTao) resutlQ.get(0);
            long id = ligneTao.getGid();
            Query q2 = em.createQuery("from CoupureLigne rd where rd.ligneTaoConcerne.gid=:i order by rd.date desc");
            q2.setParameter("i", id);
            List resultQ2 = q2.getResultList();
            ArrayList<CoupureLigneDto> reponse = new ArrayList<>();
            for (Object o : resultQ2) {
                CoupureLigne c = (CoupureLigne) o;
                reponse.add(new CoupureLigneDto(c));
            }
            return reponse;
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    /*
        Méthode permettant de poster une coupure de ligne. Elle prend en paramètre l'id de la ligne et le dto de la coupure de ligne
     */
    @Transactional
    public void postCoupureLigne(String routeId, CoupureLigneDto coupureLigneDto, String accessToken) throws ExpiredTokenException, InvalidTokenException {
        String token = tokenService.checkToken(accessToken);
        Utilisateur u = em.find(Utilisateur.class, token);
        Query q = em.createQuery("from LigneTao l where l.routeId=:r");
        q.setParameter("r", routeId);
        List resutlQ = q.getResultList();
        LigneTao ligneTao = (LigneTao) resutlQ.get(0);
        coupureLigneDto.setDeclarateurCoupure(u);
        coupureLigneDto.setLigneTaoConcerne(ligneTao);
        CoupureLigne coupureLigne = new CoupureLigne(coupureLigneDto);
        em.persist(coupureLigne);
    }

    /*
        Methode permettant de récupèrer la liste des arrets sur une ligne en utilisanr l'id de la ligne concernée
     */
    public Collection<ArretTaoDto> getArret(String routeId) {
        try {
            Query q = em.createQuery("from ArretTao a where a.stopId like CONCAT('%',:id,'%')");
            q.setParameter("id", routeId);
            List resultQ = q.getResultList();
            ArrayList<ArretTaoDto> arretTaoDtos = new ArrayList<>();
            for (Object o : resultQ) {
                ArretTao a = (ArretTao) o;
                arretTaoDtos.add(new ArretTaoDto(a));
            }
            return arretTaoDtos;
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    /*
        Méthode permettant de récupérer les dégradations faites sur une station de vélo en utilisant l'id de la station vélo
     */
    public Collection<DegradationStationVeloDto> getDegradationStationVelo(long id) {
        try {
            ArrayList<DegradationStationVeloDto> degradationStationVeloDtos = new ArrayList<>();
            List<DegradationStationVelo> degradationStationVelos = degradationStationVeloRepository.findByStationVeloDegrade_IdOrderByIdDegradationStationVeloDesc(id);
            for (DegradationStationVelo dsv : degradationStationVelos) {
                Utilisateur u = dsv.getDeclarateurDegradationStationVelo();
                degradationStationVeloDtos.add(new DegradationStationVeloDto(dsv));
            }
            return degradationStationVeloDtos;
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    /*
        Mèthode permettant de poster des degradations sur une station de vélo  en utilisant l'id de la station vèlo ainsi que le dto de la degradation la concernant
     */
    @Transactional
    public void postDegradationStationVelo(long id, DegradationStationVeloDto degradationStationVeloDto, String accessToken) throws ExpiredTokenException, InvalidTokenException {
        String token = tokenService.checkToken(accessToken);
        Utilisateur u = em.find(Utilisateur.class, token);
        Query q = em.createQuery("from StationVelo sv where sv.id=:i");
        q.setParameter("i", id);
        List resultQ = q.getResultList();
        StationVelo stationVelo = (StationVelo) resultQ.get(0);
        degradationStationVeloDto.setStationVeloDegrade(stationVelo);
        DegradationStationVelo dsv = new DegradationStationVelo(degradationStationVeloDto, u);
        em.persist(dsv);
    }

    /*
       Méthode permettant de récupérer les dégradations faites sur un stationnement de vélo en utilisant l'id du stationnement vélo
    */
    public Collection<DegradationStationnementVeloDto> getDegradationStationnementVelo(String numero) {
        try {
            List<DegradationStationnementVelo> degradationStationnementVelos = degradationStationnementVeloRepository.findByStationnementVeloDegrade_NumeroOrderByIdDegradationStationnementVeloDesc(numero);
            ArrayList<DegradationStationnementVeloDto> degradationStationnementVeloDtos = new ArrayList<>();
            for (DegradationStationnementVelo dsv : degradationStationnementVelos) {
                degradationStationnementVeloDtos.add(new DegradationStationnementVeloDto(dsv));
            }
            return degradationStationnementVeloDtos;
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    /*
       Mèthode permettant de poster des degradations sur un stationnement de vélo  en utilisant l'id du stationnement vèlo ainsi que le dto de la degradation la concernant
    */
    @Transactional
    public void postDegradationStationnementVelo(String numero, DegradationStationnementVeloDto degradationStationnementVeloDto, String accessToken) throws ExpiredTokenException, InvalidTokenException {
        String token = tokenService.checkToken(accessToken);
        Utilisateur u = em.find(Utilisateur.class, token);
        Query q = em.createQuery("from StationnementVelo sv where sv.numero=:n");
        q.setParameter("n", numero);
        List resultQ = q.getResultList();
        StationnementVelo sv = (StationnementVelo) resultQ.get(0);
        degradationStationnementVeloDto.setDeclarateurDegradationStationnementVelo(u);
        degradationStationnementVeloDto.setStationnementVeloDegrade(sv);
        DegradationStationnementVelo dsv = new DegradationStationnementVelo(degradationStationnementVeloDto);
        em.persist(dsv);
    }

    /*
       Méthode permettant de récupérer les dégradations faites sur un arret en utilisant l'id de l'arret
    */
    public Collection<DegradationArretTaoDto> getDegradationArretTao(long id) {
        try {
            Query q2 = em.createQuery("from DegradationArretTao dat where dat.arretTaoDegrade.gid=:i order by dat.idDegradationArretTao desc");
            q2.setParameter("i", id);
            List resultQ2 = q2.getResultList();
            ArrayList<DegradationArretTaoDto> arretTaoDtos = new ArrayList<>();
            for (Object o : resultQ2) {
                DegradationArretTao dat = (DegradationArretTao) o;
                arretTaoDtos.add(new DegradationArretTaoDto(dat));
            }
            return arretTaoDtos;

        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    /*
       Mèthode permettant de poster des degradations sur un arret  en utilisant l'id de l'arret ainsi que le dto de la degradation le concernant
    */
    @Transactional
    public void postDegradationArretTao(long gid, DegradationArretTaoDto degradationArretTaoDto, String accessToken) throws ExpiredTokenException, InvalidTokenException {
        String token = tokenService.checkToken(accessToken);
        Utilisateur u = em.find(Utilisateur.class, token);
        Optional<ArretTao> optional = arretTaoRepository.findById(gid);
        if (optional.isPresent()) {
            ArretTao at = optional.get();
            degradationArretTaoDto.setArretTaoDegrade(at);
            degradationArretTaoDto.setDeclarateurDegradationArretTao(u);
            DegradationArretTao dat = new DegradationArretTao(degradationArretTaoDto);
            em.persist(dat);
        }
    }

    /*
       Méthode permettant de récupérer les dégradations faites sur une piste cyclable en utilisant l'id de la piste cyclable
    */
    public Collection<DegradationPisteCyclableDto> getDegradationPisteCyclable(String numero, String insee) {
        try {
            Query q = em.createQuery("from PisteCyclable pc where pc.numero=:n and pc.insee=:i");
            q.setParameter("n", numero);
            q.setParameter("i", insee);
            List resultQ = q.getResultList();
            PisteCyclable pc = (PisteCyclable) resultQ.get(0);
            long pcId = pc.getGid();
            Query q2 = em.createQuery("from DegradationPisteCyclable dpc where dpc.pisteCyclableDegradeConcerner.gid=:id");
            q2.setParameter("id", pcId);
            List resultQ2 = q2.getResultList();
            ArrayList<DegradationPisteCyclableDto> degradationPisteCyclableDtos = new ArrayList<>();
            for (Object o : resultQ2) {
                DegradationPisteCyclable dpc = (DegradationPisteCyclable) o;
                degradationPisteCyclableDtos.add(new DegradationPisteCyclableDto(dpc));
            }
            return degradationPisteCyclableDtos;
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    /*
       Mèthode permettant de poster des degradations sur une piste cyclable  en utilisant l'id de la piste cyclable ainsi que le dto de la degradation la concernant
    */
    @Transactional
    public void postDegradationPisteCyclable(String numero, String insee, DegradationPisteCyclableDto degradationPisteCyclableDto, String accessToken) throws ExpiredTokenException, InvalidTokenException {
        String token = tokenService.checkToken(accessToken);
        Utilisateur u = em.find(Utilisateur.class, token);
        Query q = em.createQuery("from PisteCyclable pc where pc.numero=:n and pc.insee=:i");
        q.setParameter("n", numero);
        q.setParameter("i", insee);
        List resultQ = q.getResultList();
        PisteCyclable pc = (PisteCyclable) resultQ.get(0);
        degradationPisteCyclableDto.setDeclarateurDegradationPisteCyclable(u);
        degradationPisteCyclableDto.setPisteCyclableDegradeConcerner(pc);
        DegradationPisteCyclable dpc = new DegradationPisteCyclable((degradationPisteCyclableDto));
        em.persist(dpc);
    }


    /*
        Méthode permettant de récupérer toute les pénuries sur les stationnement vélo en utilisant l'id du stationnement vélo
     */
    public Collection<PenurieDto> getPenurie(long id) {
        try {
            List<Penurie> penuries = penurieRepository.findAllByStationVeloEnPenurie_IdOrderByDateDesc(id);
            ArrayList<PenurieDto> penurieDtos = new ArrayList<>();
            for (Penurie p : penuries) {
                penurieDtos.add(new PenurieDto(p));
            }
            return penurieDtos;
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    /*
       Mèthode permettant de poster des pénuries sur un stationnement de vélo  en utilisant l'id du stationnement vèlo ainsi que le dto de la pénurie
    */
    @Transactional
    public void postPenurie(long id, PenurieDto penurieDto, String accessToken) throws ExpiredTokenException, InvalidTokenException {
        String token = tokenService.checkToken(accessToken);
        Utilisateur u = em.find(Utilisateur.class, token);
        TypedQuery<StationVelo> q = em.createQuery("from StationVelo sv where sv.id=:i", StationVelo.class);
        q.setParameter("i", id);
        List<StationVelo> stations = q.getResultList();

        StationVelo sv = stations.get(0);
        Penurie p = new Penurie();
        p.setDate(new Date());
        p.setDeclarateurPenurie(u);
        p.setMessage(penurieDto.getMessage());
        p.setStationVeloEnPenurie(sv);
        em.persist(p);
    }
}

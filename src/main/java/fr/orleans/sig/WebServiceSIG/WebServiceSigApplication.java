package fr.orleans.sig.WebServiceSIG;

import fr.orleans.sig.WebServiceSIG.services.GeoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebServiceSigApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebServiceSigApplication.class, args);
    }

}

package fr.orleans.sig.WebServiceSIG.modele;


import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "piste_cyclable")
public class PisteCyclable {

    @Id
    @Column(name="gid")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer gid;

    private String numero;

    private String insee;

    private String commune;

    private String quartier;

    private String rue;

    private String anneeReal;

    private String sdic;

    private String interetCo;

    private String zones;

    private String longueur;

    private String sensCycl;

    private String longCycl;

    private String typeIti;

    private String typeVoie;

    private String position;

    private String largeur;

    private String revetement;

    private String signal;

    private String jalonnemen;

    private String info;

    private String maj;

    private String numType;

    private String codcommune;


    @OneToMany(mappedBy = "pisteCyclableConcerner")
    private Collection<Note> notes;

    @OneToMany(mappedBy = "pisteCyclableDegradeConcerner")
    private Collection<DegradationPisteCyclable> degradation;


    public Integer getGid() {
        return gid;
    }

    public void setGid(Integer gid) {
        this.gid = gid;
    }


    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }


    public String getInsee() {
        return insee;
    }

    public void setInsee(String insee) {
        this.insee = insee;
    }


    public String getCommune() {
        return commune;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }


    public String getQuartier() {
        return quartier;
    }

    public void setQuartier(String quartier) {
        this.quartier = quartier;
    }


    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }


    public String getAnneeReal() {
        return anneeReal;
    }

    public void setAnneeReal(String anneeReal) {
        this.anneeReal = anneeReal;
    }


    public String getSdic() {
        return sdic;
    }

    public void setSdic(String sdic) {
        this.sdic = sdic;
    }


    public String getInteretCo() {
        return interetCo;
    }

    public void setInteretCo(String interetCo) {
        this.interetCo = interetCo;
    }


    public String getZones() {
        return zones;
    }

    public void setZones(String zones) {
        this.zones = zones;
    }


    public String getLongueur() {
        return longueur;
    }

    public void setLongueur(String longueur) {
        this.longueur = longueur;
    }


    public String getSensCycl() {
        return sensCycl;
    }

    public void setSensCycl(String sensCycl) {
        this.sensCycl = sensCycl;
    }


    public String getLongCycl() {
        return longCycl;
    }

    public void setLongCycl(String longCycl) {
        this.longCycl = longCycl;
    }


    public String getTypeIti() {
        return typeIti;
    }

    public void setTypeIti(String typeIti) {
        this.typeIti = typeIti;
    }


    public String getTypeVoie() {
        return typeVoie;
    }

    public void setTypeVoie(String typeVoie) {
        this.typeVoie = typeVoie;
    }


    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }


    public String getLargeur() {
        return largeur;
    }

    public void setLargeur(String largeur) {
        this.largeur = largeur;
    }


    public String getRevetement() {
        return revetement;
    }

    public void setRevetement(String revetement) {
        this.revetement = revetement;
    }


    public String getSignal() {
        return signal;
    }

    public void setSignal(String signal) {
        this.signal = signal;
    }


    public String getJalonnemen() {
        return jalonnemen;
    }

    public void setJalonnemen(String jalonnemen) {
        this.jalonnemen = jalonnemen;
    }


    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }


    public String getMaj() {
        return maj;
    }

    public void setMaj(String maj) {
        this.maj = maj;
    }


    public String getNumType() {
        return numType;
    }

    public void setNumType(String numType) {
        this.numType = numType;
    }


    public String getCodcommune() {
        return codcommune;
    }

    public void setCodcommune(String codcommune) {
        this.codcommune = codcommune;
    }


    public Collection<Note> getNotes() {
        return notes;
    }

    public void setNotes(Collection<Note> notes) {
        this.notes = notes;
    }


    public Collection<DegradationPisteCyclable> getDegradation() {
        return degradation;
    }

    public void setDegradation(Collection<DegradationPisteCyclable> degradation) {
        this.degradation = degradation;
    }

}

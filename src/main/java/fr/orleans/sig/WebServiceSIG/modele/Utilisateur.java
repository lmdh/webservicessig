package fr.orleans.sig.WebServiceSIG.modele;

import javax.persistence.*;
import java.util.Collection;

@Entity
public class Utilisateur {

    @Id
    private String login;

    private String mdp;

    private Byte[] imgProfil;

    @OneToMany(mappedBy = "proprietaire")
    private Collection<Note> notes;

    @OneToMany(mappedBy = "declarateurRetard")
    private Collection<Retard> retardDeclare;

    @OneToMany(mappedBy = "declarateurCoupure")
    private Collection<CoupureLigne> coupureLignesDeclare;

    @OneToMany(mappedBy = "declarateurDegradationPisteCyclable")
    private Collection<DegradationPisteCyclable> degradationPisteCyclablesDeclare;

    @OneToMany(mappedBy = "declarateurDegradationStationnementVelo")
    private Collection<DegradationStationnementVelo> degradationStationnementVelosDeclare;

    @OneToMany(mappedBy = "declarateurDegradationArretTao")
    private Collection<DegradationArretTao> degradationArretTaosDeclare;

    @OneToMany(mappedBy = "declarateurDegradationStationVelo")
    private Collection<DegradationStationVelo> degradationStationVelosDeclare;

    @OneToMany(mappedBy = "declarateurPenurie")
    private Collection<Penurie> penuriesDeclare;


    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    public Byte[] getImgProfil() {
        return imgProfil;
    }

    public void setImgProfil(Byte[] imgProfil) {
        this.imgProfil = imgProfil;
    }

    public Collection<Note> getNotes() {
        return notes;
    }

    public void setNotes(Collection<Note> notes) {
        this.notes = notes;
    }

    public Collection<Retard> getRetardDeclare() {
        return retardDeclare;
    }

    public void setRetardDeclare(Collection<Retard> retardDeclare) {
        this.retardDeclare = retardDeclare;
    }

    public Collection<CoupureLigne> getCoupureLignesDeclare() {
        return coupureLignesDeclare;
    }

    public void setCoupureLignesDeclare(Collection<CoupureLigne> coupureLignesDeclare) {
        this.coupureLignesDeclare = coupureLignesDeclare;
    }

    public Collection<DegradationPisteCyclable> getDegradationPisteCyclablesDeclare() {
        return degradationPisteCyclablesDeclare;
    }

    public void setDegradationPisteCyclablesDeclare(Collection<DegradationPisteCyclable> degradationPisteCyclablesDeclare) {
        this.degradationPisteCyclablesDeclare = degradationPisteCyclablesDeclare;
    }

    public Collection<DegradationStationnementVelo> getDegradationStationnementVelosDeclare() {
        return degradationStationnementVelosDeclare;
    }

    public void setDegradationStationnementVelosDeclare(Collection<DegradationStationnementVelo> degradationStationnementVelosDeclare) {
        this.degradationStationnementVelosDeclare = degradationStationnementVelosDeclare;
    }

    public Collection<DegradationArretTao> getDegradationArretTaosDeclare() {
        return degradationArretTaosDeclare;
    }

    public void setDegradationArretTaosDeclare(Collection<DegradationArretTao> degradationArretTaosDeclare) {
        this.degradationArretTaosDeclare = degradationArretTaosDeclare;
    }

    public Collection<DegradationStationVelo> getDegradationStationVelosDeclare() {
        return degradationStationVelosDeclare;
    }

    public void setDegradationStationVelosDeclare(Collection<DegradationStationVelo> degradationStationVelosDeclare) {
        this.degradationStationVelosDeclare = degradationStationVelosDeclare;
    }

    public Collection<Penurie> getPenuriesDeclare() {
        return penuriesDeclare;
    }

    public void setPenuriesDeclare(Collection<Penurie> penuriesDeclare) {
        this.penuriesDeclare = penuriesDeclare;
    }
}

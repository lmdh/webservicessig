package fr.orleans.sig.WebServiceSIG.modele;

import javax.persistence.*;
import java.util.Collection;

@Entity
public class StationVelo {

  @Id
  @Column(name="gid")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long gid;

  private long id;

  private String nomstation;

  private String latitude;

  private String longitude;

  private String numvoie;

  private String voie;

  private String nomvoie;

  private String codepostal;

  private String codeinsee;

  private String commune;

  private String numplace;

  @OneToMany(mappedBy = "stationVeloDegrade")
  private Collection<DegradationStationVelo> degradations;

  @OneToMany(mappedBy = "stationVeloEnPenurie")
  private Collection<Penurie> penuriesDeclare;


  public long getGid() {
    return gid;
  }

  public void setGid(long gid) {
    this.gid = gid;
  }


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }


  public String getNomstation() {
    return nomstation;
  }

  public void setNomstation(String nomstation) {
    this.nomstation = nomstation;
  }


  public String getLatitude() {
    return latitude;
  }

  public void setLatitude(String latitude) {
    this.latitude = latitude;
  }


  public String getLongitude() {
    return longitude;
  }

  public void setLongitude(String longitude) {
    this.longitude = longitude;
  }


  public String getNumvoie() {
    return numvoie;
  }

  public void setNumvoie(String numvoie) {
    this.numvoie = numvoie;
  }


  public String getVoie() {
    return voie;
  }

  public void setVoie(String voie) {
    this.voie = voie;
  }


  public String getNomvoie() {
    return nomvoie;
  }

  public void setNomvoie(String nomvoie) {
    this.nomvoie = nomvoie;
  }


  public String getCodepostal() {
    return codepostal;
  }

  public void setCodepostal(String codepostal) {
    this.codepostal = codepostal;
  }


  public String getCodeinsee() {
    return codeinsee;
  }

  public void setCodeinsee(String codeinsee) {
    this.codeinsee = codeinsee;
  }


  public String getCommune() {
    return commune;
  }

  public void setCommune(String commune) {
    this.commune = commune;
  }


  public String getNumplace() {
    return numplace;
  }

  public void setNumplace(String numplace) {
    this.numplace = numplace;
  }

  public Collection<DegradationStationVelo> getDegradations() {
    return degradations;
  }

  public void setDegradations(Collection<DegradationStationVelo> degradations) {
    this.degradations = degradations;
  }

  public Collection<Penurie> getPenuriesDeclare() {
    return penuriesDeclare;
  }

  public void setPenuriesDeclare(Collection<Penurie> penuriesDeclare) {
    this.penuriesDeclare = penuriesDeclare;
  }
}

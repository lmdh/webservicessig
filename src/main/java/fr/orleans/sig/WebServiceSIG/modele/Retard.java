package fr.orleans.sig.WebServiceSIG.modele;

import fr.orleans.sig.WebServiceSIG.dto.RetardDto;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Retard {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idRetard;

    private String message;

    private long duree;

    private long idArret;

    private Date date;

    @ManyToOne
    private Utilisateur declarateurRetard;

    @ManyToOne
    private LigneTao ligneTaoConcerneRetard;

    public Retard (){}

    public Retard(RetardDto retardDto){
        message = retardDto.getMessage();
        duree = retardDto.getDuree();
        idArret = retardDto.getIdArret();
        date = retardDto.getDate();
        declarateurRetard = retardDto.getDeclarateurRetard();
        ligneTaoConcerneRetard = retardDto.getLigneTaoConcerneRetard();
    }

    public long getIdRetard() {
        return idRetard;
    }

    public void setIdRetard(long idRetard) {
        this.idRetard = idRetard;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getDuree() {
        return duree;
    }

    public void setDuree(long duree) {
        this.duree = duree;
    }

    public long getIdArret() {
        return idArret;
    }

    public void setIdArret(long idArret) {
        this.idArret = idArret;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Utilisateur getDeclarateurRetard() {
        return declarateurRetard;
    }

    public void setDeclarateurRetard(Utilisateur declarateurRetard) {
        this.declarateurRetard = declarateurRetard;
    }

    public LigneTao getLigneTaoConcerneRetard() {
        return ligneTaoConcerneRetard;
    }

    public void setLigneTaoConcerneRetard(LigneTao ligneTaoConcerneRetard) {
        this.ligneTaoConcerneRetard = ligneTaoConcerneRetard;
    }
}

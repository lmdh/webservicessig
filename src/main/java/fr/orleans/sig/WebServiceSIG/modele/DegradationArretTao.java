package fr.orleans.sig.WebServiceSIG.modele;

import fr.orleans.sig.WebServiceSIG.dto.DegradationArretTaoDto;

import javax.persistence.*;
import java.util.Date;

@Entity
public class DegradationArretTao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idDegradationArretTao;

    private String message;

    private Date date;

    private byte[] photo;

    @ManyToOne
    private Utilisateur declarateurDegradationArretTao;

    @ManyToOne
    private ArretTao arretTaoDegrade;

    public DegradationArretTao(){}

    public DegradationArretTao(DegradationArretTaoDto datd){
        idDegradationArretTao = datd.getIdDegradationArretTao();
        message = datd.getMessage();
        date = datd.getDate();
        photo = datd.getPhoto();
        declarateurDegradationArretTao = datd.getDeclarateurDegradationArretTao();
        arretTaoDegrade = datd.getArretTaoDegrade();
    }

    public DegradationArretTao(String message, Date date, byte[] photo){
        this.message = message;
        this.date = date;
        this.photo = photo;
    }

    public long getIdDegradationArretTao() {
        return idDegradationArretTao;
    }

    public void setIdDegradationArretTao(long idDegradationArretTao) {
        this.idDegradationArretTao = idDegradationArretTao;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public Utilisateur getDeclarateurDegradationArretTao() {
        return declarateurDegradationArretTao;
    }

    public void setDeclarateurDegradationArretTao(Utilisateur declarateurDegradationArretTao) {
        this.declarateurDegradationArretTao = declarateurDegradationArretTao;
    }

    public ArretTao getArretTaoDegrade() {
        return arretTaoDegrade;
    }

    public void setArretTaoDegrade(ArretTao arretTaoDegrade) {
        this.arretTaoDegrade = arretTaoDegrade;
    }
}

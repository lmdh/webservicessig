package fr.orleans.sig.WebServiceSIG.modele;

import fr.orleans.sig.WebServiceSIG.dto.CoupureLigneDto;

import javax.persistence.*;
import java.util.Date;

@Entity
public class CoupureLigne {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idCoupureLigne;

    private String message;

    private long idArretDepart;

    private long idArretArrivee;

    private Date date;

    @ManyToOne
    private Utilisateur declarateurCoupure;

    @ManyToOne
    private LigneTao ligneTaoConcerne;

    public CoupureLigne(){}

    public CoupureLigne(CoupureLigneDto c){
        message = c.getMessage();
        idArretDepart = c.getIdArretDepart();
        idArretArrivee = c.getIdArretArrivee();
        date = c.getDate();
        declarateurCoupure = c.getDeclarateurCoupure();
        ligneTaoConcerne = c.getLigneTaoConcerne();
    }

    public long getIdCoupureLigne() {
        return idCoupureLigne;
    }

    public void setIdCoupureLigne(long idCoupureLigne) {
        this.idCoupureLigne = idCoupureLigne;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getIdArretDepart() {
        return idArretDepart;
    }

    public void setIdArretDepart(long idArretDepart) {
        this.idArretDepart = idArretDepart;
    }

    public long getIdArretArrivee() {
        return idArretArrivee;
    }

    public void setIdArretArrivee(long idArretArrivee) {
        this.idArretArrivee = idArretArrivee;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Utilisateur getDeclarateurCoupure() {
        return declarateurCoupure;
    }

    public void setDeclarateurCoupure(Utilisateur declarateurCoupure) {
        this.declarateurCoupure = declarateurCoupure;
    }

    public LigneTao getLigneTaoConcerne() {
        return ligneTaoConcerne;
    }

    public void setLigneTaoConcerne(LigneTao ligneTaoConcerne) {
        this.ligneTaoConcerne = ligneTaoConcerne;
    }
}

package fr.orleans.sig.WebServiceSIG.modele;


import javax.persistence.*;
import java.util.Collection;

@Entity
public class LigneTao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long gid;
    private String routeId;
    private String shortName;
    private String longName;
    private String type;
    private String color;
    private String geom;
    @OneToMany(mappedBy = "ligneTaoConcerne")
    private Collection<CoupureLigne> coupureLigne;
    @OneToMany(mappedBy = "ligneTaoConcerneRetard")
    private Collection<Retard> retardLigne;



    public long getGid() {
        return gid;
    }

    public void setGid(long gid) {
        this.gid = gid;
    }


    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }


    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }


    public String getLongName() {
        return longName;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }


    public String getGeom() {
        return geom;
    }

    public void setGeom(String geom) {
        this.geom = geom;
    }

}

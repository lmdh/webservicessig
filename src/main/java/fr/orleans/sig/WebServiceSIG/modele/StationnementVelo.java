package fr.orleans.sig.WebServiceSIG.modele;

import javax.persistence.*;
import java.util.Collection;

@Entity
public class StationnementVelo {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long gid;

  private String numero;

  private String commune;

  private String insee;

  private String quartier;

  private String rue;

  private String anneeReal;

  private String lieu;

  private String type;

  private String nbPlaces;

  private String couverture;

  private String gardien;

  private String panneau;

  private String info;

  private String maj;

  @OneToMany(mappedBy = "stationnementVeloDegrade")
  private Collection<DegradationStationnementVelo> degradations;


  public long getGid() {
    return gid;
  }

  public void setGid(long gid) {
    this.gid = gid;
  }


  public String getNumero() {
    return numero;
  }

  public void setNumero(String numero) {
    this.numero = numero;
  }


  public String getCommune() {
    return commune;
  }

  public void setCommune(String commune) {
    this.commune = commune;
  }


  public String getInsee() {
    return insee;
  }

  public void setInsee(String insee) {
    this.insee = insee;
  }


  public String getQuartier() {
    return quartier;
  }

  public void setQuartier(String quartier) {
    this.quartier = quartier;
  }


  public String getRue() {
    return rue;
  }

  public void setRue(String rue) {
    this.rue = rue;
  }


  public String getAnneeReal() {
    return anneeReal;
  }

  public void setAnneeReal(String anneeReal) {
    this.anneeReal = anneeReal;
  }


  public String getLieu() {
    return lieu;
  }

  public void setLieu(String lieu) {
    this.lieu = lieu;
  }


  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }


  public String getNbPlaces() {
    return nbPlaces;
  }

  public void setNbPlaces(String nbPlaces) {
    this.nbPlaces = nbPlaces;
  }


  public String getCouverture() {
    return couverture;
  }

  public void setCouverture(String couverture) {
    this.couverture = couverture;
  }


  public String getGardien() {
    return gardien;
  }

  public void setGardien(String gardien) {
    this.gardien = gardien;
  }


  public String getPanneau() {
    return panneau;
  }

  public void setPanneau(String panneau) {
    this.panneau = panneau;
  }


  public String getInfo() {
    return info;
  }

  public void setInfo(String info) {
    this.info = info;
  }


  public String getMaj() {
    return maj;
  }

  public void setMaj(String maj) {
    this.maj = maj;
  }

  public Collection<DegradationStationnementVelo> getDegradations() {
    return degradations;
  }

  public void setDegradations(Collection<DegradationStationnementVelo> degradations) {
    this.degradations = degradations;
  }
}

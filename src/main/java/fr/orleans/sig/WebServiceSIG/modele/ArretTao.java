package fr.orleans.sig.WebServiceSIG.modele;

import org.locationtech.jts.geom.Point;

import javax.persistence.*;
import java.util.Collection;

@Entity
public class ArretTao {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long gid;

  private String stopId;

  private String stopname;

  private String location_t;

  private String parentSta;

  private Point geom;

  @OneToMany(mappedBy = "arretTaoDegrade")
  private Collection<DegradationArretTao> degradations;


  public long getGid() {
    return gid;
  }

  public void setGid(long gid) {
    this.gid = gid;
  }


  public String getStopId() {
    return stopId;
  }

  public void setStopId(String stopId) {
    this.stopId = stopId;
  }


  public String getStopname() {
    return stopname;
  }

  public void setStopname(String stopname) {
    this.stopname = stopname;
  }


  public String getLocation_t() {
    return location_t;
  }

  public void setLocation_t(String location_t) {
    this.location_t = location_t;
  }


  public String getParentSta() {
    return parentSta;
  }

  public void setParentSta(String parentSta) {
    this.parentSta = parentSta;
  }


  public Collection<DegradationArretTao> getDegradations() {
    return degradations;
  }

  public void setDegradations(Collection<DegradationArretTao> degradations) {
    this.degradations = degradations;
  }
}

package fr.orleans.sig.WebServiceSIG.modele;

import fr.orleans.sig.WebServiceSIG.dto.DegradationStationVeloDto;

import javax.persistence.*;
import java.util.Date;

@Entity
public class DegradationStationVelo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idDegradationStationVelo;

    private String message;

    private Date date;

    private byte[] photo;

    @ManyToOne()
    private Utilisateur declarateurDegradationStationVelo;

    @ManyToOne()
    private StationVelo stationVeloDegrade;

    public DegradationStationVelo(){}

    public DegradationStationVelo(DegradationStationVeloDto dsvd, Utilisateur u){
        this.message = dsvd.getMessage();
        this.date = dsvd.getDate();
        this.photo= dsvd.getPhoto();
        this.declarateurDegradationStationVelo = u;
        this.stationVeloDegrade = dsvd.getStationVeloDegrade();
    }

    public long getIdDegradationStationVelo() {
        return idDegradationStationVelo;
    }

    public void setIdDegradationStationVelo(long idDegradationStationVelo) {
        this.idDegradationStationVelo = idDegradationStationVelo;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public Utilisateur getDeclarateurDegradationStationVelo() {
        return declarateurDegradationStationVelo;
    }

    public void setDeclarateurDegradationStationVelo(Utilisateur declarateurDegradationStationVelo) {
        declarateurDegradationStationVelo = declarateurDegradationStationVelo;
    }

    public StationVelo getStationVeloDegrade() {
        return stationVeloDegrade;
    }

    public void setStationVeloDegrade(StationVelo stationVeloDegrade) {
        this.stationVeloDegrade = stationVeloDegrade;
    }
}

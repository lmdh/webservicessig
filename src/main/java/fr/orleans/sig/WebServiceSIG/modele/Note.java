package fr.orleans.sig.WebServiceSIG.modele;

import fr.orleans.sig.WebServiceSIG.dto.NoteDto;

import javax.persistence.*;

@Entity
public class Note {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idNote;

    private float note;

    @ManyToOne
    private PisteCyclable pisteCyclableConcerner;

    @ManyToOne
    private Utilisateur proprietaire;

    public Note(){}

    public Note(int note){
        this.note = note;
    }

    public Note(NoteDto nd){
        idNote = nd.getIdNote();
        note = nd.getNote();
        pisteCyclableConcerner = nd.getPisteCyclableConcerner();
        proprietaire = nd.getProprietaire();
    }

    public long getIdNote() {
        return idNote;
    }

    public void setIdNote(long idNote) {
        this.idNote = idNote;
    }

    public float getNote() {
        return note;
    }

    public void setNote(float note) {
        this.note = note;
    }

    public PisteCyclable getPisteCyclableConcerner() {
        return pisteCyclableConcerner;
    }

    public void setPisteCyclableConcerner(PisteCyclable pisteCyclableConcerner) {
        this.pisteCyclableConcerner = pisteCyclableConcerner;
    }

    public Utilisateur getProprietaire() {
        return proprietaire;
    }

    public void setProprietaire(Utilisateur proprietaire) {
        this.proprietaire = proprietaire;
    }
}

package fr.orleans.sig.WebServiceSIG.modele;

import fr.orleans.sig.WebServiceSIG.dto.DegradationPisteCyclableDto;

import javax.persistence.*;
import java.util.Date;

@Entity
public class DegradationPisteCyclable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idDegradationPisteCyclable;

    private String message;

    private Date date;

    private byte[] photo;

    @ManyToOne
    private Utilisateur declarateurDegradationPisteCyclable;

    @ManyToOne
    private PisteCyclable pisteCyclableDegradeConcerner;

    public DegradationPisteCyclable(){}

    public DegradationPisteCyclable(String message, Date date, String photo){
        this.message = message;
        this.date = date;
        this.photo = photo.getBytes();
    }

    public DegradationPisteCyclable(DegradationPisteCyclableDto dpcd){
        idDegradationPisteCyclable = dpcd.getIdDegradationPisteCyclable();
        message = dpcd.getMessage();
        date = dpcd.getDate();
        photo = dpcd.getPhoto();
        declarateurDegradationPisteCyclable = dpcd.getDeclarateurDegradationPisteCyclable();
        pisteCyclableDegradeConcerner = dpcd.getPisteCyclableDegradeConcerner();
    }

    public long getIdDegradationPisteCyclable() {
        return idDegradationPisteCyclable;
    }

    public void setIdDegradationPisteCyclable(long idDegradationPisteCyclable) {
        this.idDegradationPisteCyclable = idDegradationPisteCyclable;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public Utilisateur getDeclarateurDegradationPisteCyclable() {
        return declarateurDegradationPisteCyclable;
    }

    public void setDeclarateurDegradationPisteCyclable(Utilisateur declarateurDegradationPisteCyclable) {
        this.declarateurDegradationPisteCyclable = declarateurDegradationPisteCyclable;
    }

    public PisteCyclable getPisteCyclableDegradeConcerner() {
        return pisteCyclableDegradeConcerner;
    }

    public void setPisteCyclableDegradeConcerner(PisteCyclable pisteCyclableDegradeConcerner) {
        this.pisteCyclableDegradeConcerner = pisteCyclableDegradeConcerner;
    }
}

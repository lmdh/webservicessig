package fr.orleans.sig.WebServiceSIG.modele;

import fr.orleans.sig.WebServiceSIG.dto.DegradationStationnementVeloDto;

import javax.persistence.*;
import java.util.Date;

@Entity
public class DegradationStationnementVelo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idDegradationStationnementVelo;

    private String message;

    private Date date;

    private byte[] photo;

    @ManyToOne
    private Utilisateur declarateurDegradationStationnementVelo;

    @ManyToOne
    private StationnementVelo stationnementVeloDegrade;

    public DegradationStationnementVelo(){}

    public DegradationStationnementVelo(DegradationStationnementVeloDto dsvd){
        message = dsvd.getMessage();
        date = dsvd.getDate();
        photo =dsvd.getPhoto();
        declarateurDegradationStationnementVelo = dsvd.getDeclarateurDegradationStationnementVelo();
        stationnementVeloDegrade = dsvd.getStationnementVeloDegrade();
    }

    public DegradationStationnementVelo(String message, Date date, String  photo) {
        this.message = message;
        this.date = date;
        this.photo = photo.getBytes();
    }

    public long getIdDegradationStationnementVelo() {
        return idDegradationStationnementVelo;
    }

    public void setIdDegradationStationnementVelo(long idDegradationStationnementVelo) {
        this.idDegradationStationnementVelo = idDegradationStationnementVelo;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public Utilisateur getDeclarateurDegradationStationnementVelo() {
        return declarateurDegradationStationnementVelo;
    }

    public void setDeclarateurDegradationStationnementVelo(Utilisateur declarateurDegradationStationnementVelo) {
        this.declarateurDegradationStationnementVelo = declarateurDegradationStationnementVelo;
    }

    public StationnementVelo getStationnementVeloDegrade() {
        return stationnementVeloDegrade;
    }

    public void setStationnementVeloDegrade(StationnementVelo stationnementVeloDegrade) {
        this.stationnementVeloDegrade = stationnementVeloDegrade;
    }
}

package fr.orleans.sig.WebServiceSIG.modele;

import fr.orleans.sig.WebServiceSIG.dto.PenurieDto;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Penurie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idPenurie;

    private String message;

    private Date date;

    @ManyToOne
    private Utilisateur declarateurPenurie;

    @ManyToOne
    private StationVelo stationVeloEnPenurie;

    public Penurie(){}

    public Penurie(String message, Date date){
        this.message = message;
        this.date = date;
    }

    public Penurie(PenurieDto pdto){
        idPenurie = pdto.getIdPenurie();
        message = pdto.getMessage();
        date = pdto.getDate();
        /*declarateurPenurie = pdto.getDeclarateurPenurie();
        stationVeloEnPenurie = pdto.getStationVeloEnPenurie();*/
    }

    public long getIdPenurie() {
        return idPenurie;
    }

    public void setIdPenurie(long idPenurie) {
        this.idPenurie = idPenurie;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Utilisateur getDeclarateurPenurie() {
        return declarateurPenurie;
    }

    public void setDeclarateurPenurie(Utilisateur declarateurPenurie) {
        this.declarateurPenurie = declarateurPenurie;
    }

    public StationVelo getStationVeloEnPenurie() {
        return stationVeloEnPenurie;
    }

    public void setStationVeloEnPenurie(StationVelo stationVeloEnPenurie) {
        this.stationVeloEnPenurie = stationVeloEnPenurie;
    }
}
